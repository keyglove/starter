import serial
import time
from functools import reduce
from operator import sub

def subtract_stringified_list(stringifiedList):
    return reduce(sub, list(map(int, stringifiedList.split(" ")))[::-1])

# Open serial port
ser = serial.Serial("/dev/ttyACM5", 9600)

# Read and print serial data

print(ser.read_until(b"start\n").decode("utf-8"))

while True:
    start = time.time_ns() / 1000000
    ser.write(b"\n")
    stringified_list_from_arduino = ser.read_until(b"\n").decode("utf-8")
    print(((time.time_ns() / 1000000) - start) - subtract_stringified_list(stringified_list_from_arduino))
