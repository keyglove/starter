from multiprocessing import Process, Queue
from utils.multiprocessing_communication import put_in_queue_or_log, KeygloveEvent, auto_printify
import serial
import time

@auto_printify
class SingleGyroData:
    def __init__(self, x: int, y: int, z: int):
        self.x = x
        self.y = y
        self.z = z

GyroDataCollection = dict[int, SingleGyroData]

@auto_printify
class TimeSyncEventData:
    def __init__(self, estimated_delay_between_local_and_device: float):
        self.estimated_delay_between_local_and_device = estimated_delay_between_local_and_device

@auto_printify
class GyroEventData:
    def __init__(self, gyro_data_collection: GyroDataCollection):
        self.gyro_data_collection = gyro_data_collection

def start_arduino_interaction(queue: Queue, max_gyros: int = 5, serial_port: str = "/dev/ttyACM0", baud_rate: int = 115200):
    ser = serial.Serial(serial_port, baud_rate)

    estimated_delay_between_local_and_device = 0

    gyro_data_collection: GyroDataCollection = {}

    print("Waiting for handshake...")
    ser.read_until(b"start\n")
    print("Handshake received")
    local_timestamp_epoch = time.time_ns() / 1000000
    ser.write(b"\n")

    print("Starting serial read loop")
    while True:
        serial_data = ser.read(7)
        if serial_data[0] == 10:
            time_delta_local = time.time_ns() / 1000000 - local_timestamp_epoch
            time_delta_device = int.from_bytes(
                serial_data[1:5], byteorder="little", signed=False
            )

            estimated_delay_between_local_and_device = (
                time_delta_local - time_delta_device
            ) / 2

            put_in_queue_or_log(
                queue,
                KeygloveEvent(
                    TimeSyncEventData(estimated_delay_between_local_and_device),
                    time.time_ns(),
                ),
            )
        elif serial_data[0] >= 0 and serial_data[0] <= max_gyros:
            gyro_id = serial_data[0]
            if gyro_id in gyro_data_collection:
                gyro_data_collection = {}

            gyro_data_collection[gyro_id] = SingleGyroData(
                x=int.from_bytes(serial_data[1:3], byteorder="little", signed=True),
                y=int.from_bytes(serial_data[3:5], byteorder="little", signed=True),
                z=int.from_bytes(serial_data[5:7], byteorder="little", signed=True),
            )
            if len(gyro_data_collection) == max_gyros:
                put_in_queue_or_log(queue, KeygloveEvent(GyroEventData(gyro_data_collection), time.time_ns()))
                gyro_data_collection = {}

if __name__ == "__main__":
    queue = Queue()
    arduino_process = Process(target=start_arduino_interaction, args=(queue,))
    arduino_process.start()

    while True:
        print(queue.get())