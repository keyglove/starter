from multiprocessing import Process, Queue
from pickle import dumps
from arduino import start_arduino_interaction
from keyboard.capture import start_keyboard_capture
from utils.multiprocessing_communication import KeygloveEvent
from time import time
from math import floor
import atexit
from gzip import compress

if __name__ == "__main__":
    data: list[KeygloveEvent] = []
    def exit_handler():
        with open(str(floor(time())) + "-data.pickle.gz", "wb") as f:
            f.write(compress(dumps(data)))
            f.close()

    queue = Queue()
    arduino_process = Process(target=start_arduino_interaction, args=(queue,))
    keyboard_process = Process(target=start_keyboard_capture, args=(queue,))
    arduino_process.start()
    keyboard_process.start()

    atexit.register(exit_handler)

    while True:
        event: KeygloveEvent = queue.get()
        data.append(event.data)
