# Communication Buffer Structure

<table style="text-align: center">
		<thead>
				<tr>
						<th>Data type</th>
						<th>Byte #1</th>
						<th>Byte #2</th>
						<th>Byte #3</th>
						<th>Byte #4</th>
						<th>Byte #5</th>
						<th>Byte #6</th>
						<th>Byte #7</th>
				</tr>
		</thead>
		<tbody>
				<tr>
						<td>Accelerometer data</td>
						<td colspan=2> Value 1 </td>
						<td colspan=2> Value 2 </td>
						<td colspan=2> Value 3 </td>
						<td>Hand index [0-9]</td>
				</tr>
				<tr>
						<td>Milliseconds since start</td>
						<td colspan=4> Milliseconds </td>
						<td>0 (constant)</td>
						<td>0 (constant)</td>
						<td>10 (constant)</td>
				</tr>
		</tbody>
</table>
Note: all values are unsigned integers.


