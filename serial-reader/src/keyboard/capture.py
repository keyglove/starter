from multiprocessing import Queue, Process
from utils.multiprocessing_communication import put_in_queue_or_log, KeygloveEvent, auto_printify
from pynput import keyboard
import time
import enum

@auto_printify
class KeyAction(enum.Enum):
    UP = "up"
    DOWN = "down"

@auto_printify
class KeyboardEventData:
    def __init__(self, key: (keyboard.Key | keyboard.KeyCode), action: KeyAction):
        self.key = key
        self.action = action

def start_keyboard_capture(queue: Queue):
    def on_press(key: (keyboard.Key | keyboard.KeyCode | None)):
        if key != None:
            put_in_queue_or_log(queue, KeygloveEvent(KeyboardEventData(key, KeyAction.UP), time.time_ns()))

    def on_release(key: (keyboard.Key | keyboard.KeyCode | None)):
        if key != None:
            put_in_queue_or_log(queue, KeygloveEvent(KeyboardEventData(key, KeyAction.DOWN), time.time_ns()))

    # Set up the keyboard event listener
    listener = keyboard.Listener(on_press=on_press, on_release=on_release)
    listener.start()
    listener.join()

    # while True:
    #     pass

if __name__ == "__main__":
    queue = Queue()
    keyboard_process = Process(target=start_keyboard_capture, args=(queue,))
    keyboard_process.start()
    while True:
        print(queue.get())
