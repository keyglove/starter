from multiprocessing import Queue
from typing import Generic, TypeVar

def auto_printify(cls):
    def __repr__(self):
        return f'{self.__class__.__name__}({self.__dict__!r})'
    cls.__repr__ = __repr__
    return cls

T = TypeVar("T")

@auto_printify
class KeygloveEvent(Generic[T]):
    def __init__(self, data: T, time: int):
        self.time = time
        self.data = data

def put_in_queue_or_log(queue, data: KeygloveEvent):
    try:
        queue.put_nowait(data)
    except Queue.full:
        print("Queue full")
