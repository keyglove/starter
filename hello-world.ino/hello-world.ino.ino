// (c) Michael Schoeffler 2017, http://www.mschoeffler.de

#include "Wire.h" // This library allows you to communicate with I2C devices.
#include "./flags.h"

#define sizeof_bits(x) (sizeof(x)*8)

uint8_t accel_config;

struct Gyro {
  int16_t x;
  int16_t y;
  int16_t z;
};

const size_t num_gyros = 5;
Gyro gyro;
size_t active_gyro = 0;
size_t gyro_to_print = 0;

unsigned long device_timestamp_epoch;
unsigned long time_since_epoch;
unsigned long time_update_counter = 0;
#define sync_heart_beat 1000

uint8_t communication_buffer[7];

#define MPU_ACTIVE_ADDRESS 0x69

#define MPU_ADDRESS_START_PIN 2

void read_wire(int16_t* wire_value) {
  if (wire_value == NULL) {
    return;
  }

  (*wire_value) = Wire.read();
  (*wire_value) <<= 8;
  (*wire_value) |= Wire.read();
}

void read_gyro(Gyro *gyro) {
  Wire.beginTransmission(MPU_ACTIVE_ADDRESS);
  Wire.write(0x43); // starting with register 0x3B (ACCEL_XOUT_H) [MPU-6000 and MPU-6050 Register Map and Descriptions Revision 4.2, p.40]
  Wire.endTransmission(false);
  Wire.requestFrom((uint8_t) MPU_ACTIVE_ADDRESS, (size_t) 3*2, true);
  
  read_wire(&gyro->x);
  read_wire(&gyro->y);
  read_wire(&gyro->z);
}

void set_gyro_config() {
  Wire.beginTransmission(MPU_ACTIVE_ADDRESS);
  Wire.write(0x1B); // starting with register 0x1B (GYRO_CONFIG)
  Wire.write(0b00011000); // set full scale range to 2000 deg/s
  Wire.endTransmission(true);
}

void print_gyro_config() {
  Wire.beginTransmission(MPU_ACTIVE_ADDRESS);
  Wire.write(0x1B); // starting with register 0x1B (GYRO_CONFIG)
  Wire.endTransmission(false);
  Wire.requestFrom((uint8_t) MPU_ACTIVE_ADDRESS, (size_t) 1, true);

  accel_config = Wire.read();

  Serial.print("XG_ST: ");
  Serial.println((accel_config & 0x80) >> 7);

  Serial.print("YG_ST: ");
  Serial.println((accel_config & 0x40) >> 6);

  Serial.print("ZG_ST: ");
  Serial.println((accel_config & 0x20) >> 5);

  Serial.print("FS_SEL: ");
  Serial.println((accel_config & 0x18) >> 3);
}

void turn_on_mpu() {
  Wire.begin();
  Wire.beginTransmission(MPU_ACTIVE_ADDRESS);
  Wire.write(0x6B); // PWR_MGMT_1 register
  Wire.write(0); // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);
}

void set_active_gyro(size_t gyro_index) {
  if(gyro_index >= num_gyros || gyro_index < 0) {
    return;
  }

  for(size_t i = 0; i < num_gyros; i++) {
    digitalWrite(
      MPU_ADDRESS_START_PIN + i,
      i != gyro_index ? LOW : HIGH
    );
  }
}

void activate_address_pins() {
  for(size_t i = 0; i < num_gyros; i++) {
    pinMode(MPU_ADDRESS_START_PIN + i, OUTPUT);
  }
}

void setup() {
  Serial.begin(115200);

  activate_address_pins();

  for (size_t i = 0; i < num_gyros; i++) {
    set_active_gyro(i);
    turn_on_mpu();
    set_gyro_config();
    #ifdef DEBUG
    print_gyro_config();
    #endif
  }

  Serial.print("start\n");
  while(!Serial.available()) {
    delayMicroseconds(1);
  }
  device_timestamp_epoch = millis();
}

void loop() {
  if(Serial.available() > 0) {
    gyro_to_print = Serial.parseInt()%num_gyros;
  }
  
  time_since_epoch = millis() - device_timestamp_epoch;
  if (time_since_epoch/sync_heart_beat != time_update_counter) {
    time_update_counter = time_since_epoch/sync_heart_beat;
    if (time_since_epoch>>sizeof_bits(uint32_t) == 0) {
      memset(communication_buffer, 0, sizeof(communication_buffer));

      communication_buffer[0] = (uint8_t)10;

      time_since_epoch <<= sizeof_bits(time_since_epoch) - sizeof_bits(uint32_t);

      memcpy(&(communication_buffer[1]), &time_since_epoch, sizeof(uint32_t));
      #ifndef DEBUG
      Serial.write(communication_buffer, sizeof(communication_buffer));
      #else
      Serial.print("time: ");
      Serial.println(time_since_epoch);
      #endif
    }
  }

  for (size_t i = 0; i < num_gyros; i++) {
    set_active_gyro(i);
    read_gyro(&gyro);

    memset(communication_buffer, 0, sizeof(communication_buffer));

    communication_buffer[0] = i;

    memcpy(&communication_buffer[1], &gyro.x, sizeof(gyro.x));
    memcpy(&communication_buffer[3], &gyro.y, sizeof(gyro.y));
    memcpy(&communication_buffer[5], &gyro.z, sizeof(gyro.z));
    #ifndef DEBUG
    Serial.write(communication_buffer, sizeof(communication_buffer));
    #else
    if (i == gyro_to_print) {
      Serial.print(i);
      Serial.print(" ");
      Serial.print(gyro.x);
      Serial.print(" ");
      Serial.print(gyro.y);
      Serial.print(" ");
      Serial.println(gyro.z);
    }
    #endif
  }
}